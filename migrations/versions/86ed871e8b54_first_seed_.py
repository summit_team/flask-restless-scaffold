"""first_seed_

Revision ID: 86ed871e8b54
Revises: 31c06b156abe
Create Date: 2021-03-18 12:58:45.846433

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql import table, column

import datetime
import hashlib


# revision identifiers, used by Alembic.
revision = '86ed871e8b54'
down_revision = '31c06b156abe'
branch_labels = None
depends_on = None


def upgrade():
    users_table = table('users',
                        column('id', sa.Integer),
                        column('name', sa.String),
                        column('username', sa.String),
                        column('email', sa.String),
                        column('password', sa.String),
                        column('created', sa.DateTime),
                        column('updated', sa.DateTime),
                        )

    op.bulk_insert(users_table,
                    [
                        {'id': None,'name': 'Usuerio Demo','username': 'demo','email': 'demo@demo.com','password': hashlib.sha1('demo'.encode('utf-8')).hexdigest(),'created': datetime.datetime.utcnow(),'updated': datetime.datetime.utcnow(),},

                    ]
                    )


def downgrade():
    pass
