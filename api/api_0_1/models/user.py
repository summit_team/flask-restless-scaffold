from flask import current_app as app
from db import db
import hashlib

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255), unique=True, index=True, nullable=False)
    password = db.Column(db.String(50), index=True, nullable=False)
    email = db.Column(db.String(150), index=True)
    name = db.Column(db.String(255), index=True)
    created = db.Column(db.DateTime, default=db.func.now(), nullable=False)
    updated = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())

    def __init__(self,
                 email=None,
                 username=None,
                 password=None,
                 name=None):

        self.email = email
        self.username = username
        self.password = hashlib.sha1(str(password)).hexdigest()
        self.name = name
