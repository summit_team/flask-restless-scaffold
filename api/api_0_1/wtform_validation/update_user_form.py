# -*- coding: utf-8 -*-
from wtforms import Form, StringField, IntegerField, validators, BooleanField, PasswordField

class UpdateUserForm(Form):
    name = StringField('Nombre', [validators.required(message='Este campo es obligatorio.')])
    email = StringField('email', [validators.required(message='Este campo es obligatorio.')])
    password = PasswordField('password', [validators.optional()])
