# -*- coding: utf-8 -*-
from wtforms import Form, StringField, IntegerField, validators, PasswordField, BooleanField


class UserForm(Form):
    username = StringField('username', [validators.required(message='Este campo es obligatorio.')])
    password = PasswordField('password', [validators.required(message='Este campo es obligatorio.')])
    name = StringField('name', [validators.required(message='Este campo es obligatorio.')])
    email = StringField('email', [validators.required(message='Este campo es obligatorio.')])
