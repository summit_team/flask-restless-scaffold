from flask_restless import ProcessingException

class ValidateAllowedFields:
    def __init__(self, allowed_fields, data):
        for field in data.keys():
            if field not in allowed_fields:
                raise ProcessingException(description='Bad Request')