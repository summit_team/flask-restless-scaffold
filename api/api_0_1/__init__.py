from flask_restless import APIManager
from savalidation import ValidationError
from models.db import db

from models.user import User
from resources.users import Users

manager = APIManager(flask_sqlalchemy_db=db)

Users = Users()

manager.create_api(
    User,
    methods=Users.methods,
    url_prefix=Users.url_prefix,
    preprocessors=Users.preprocessors,
    postprocessors=Users.postprocessors,
    validation_exceptions=[ValidationError]
)
