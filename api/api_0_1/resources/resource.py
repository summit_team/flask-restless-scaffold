from flask_restless import ProcessingException
from flask_jwt_extended import jwt_required



class Resource(object):
    """
    Resource
    **Overview**

    Used to generalize attributes and methods for all views package.
    Properties needed to create an endpoint using restless-extension
    """

    preprocessors = None
    postprocessors = None
    url_prefix = '/0.1'
    methods = ['GET', 'POST', 'PATCH', 'DELETE']
    include_columns = None
    exclude_columns = None

    def __init__(self):
        self.preprocessors = {
            'GET_SINGLE': [self.get_single_preprocessor],
            'GET_MANY': [self.get_many_preprocessor],
            'POST': [self.post_preprocessor],
            'PATCH_SINGLE': [self.patch_single_preprocessor],
            'PATCH_MANY': [self.patch_many_preprocessor],
            'DELETE_SINGLE': [self.delete_single_preprocessor],
            'DELETE_MANY': [self.delete_many_preprocessor]
        }

        self.postprocessors = {
            'GET_SINGLE': [self.get_single_postprocessor],
            'GET_MANY': [self.get_many_postprocessor],
            'POST': [self.post_postprocessor],
            'PATCH_SINGLE': [self.patch_single_postprocessor],
            'PATCH_MANY': [self.patch_many_postprocessor],
            'DELETE_SINGLE': [self.delete_postprocessor],
            'DELETE_MANY': [self.delete_many_postprocessor]
        }


    # Preprocessors
    @classmethod
    @jwt_required
    def get_single_preprocessor(cls, instance_id=None, **kw):
        pass

    @classmethod
    @jwt_required
    def get_many_preprocessor(cls, search_params=None, **kw):
        pass

    @classmethod
    @jwt_required
    def post_preprocessor(cls, data=None, **kw):
        pass

    @classmethod
    @jwt_required
    def patch_single_preprocessor(cls, instance_id=None, data=None, **kw):
        pass

    @classmethod
    @jwt_required
    def patch_many_preprocessor(cls, search_params=None, data=None, **kw):
        pass

    @classmethod
    @jwt_required
    def delete_single_preprocessor(cls, instance_id=None, **kw):
        pass

    @classmethod
    @jwt_required
    def delete_many_preprocessor(cls, search_params=None, **kw):
        pass

    # Postprocessors
    @classmethod
    def get_single_postprocessor(cls, result=None, **kw):
        pass

    @classmethod
    def get_many_postprocessor(cls, result=None, search_params=None, **kw):
        pass

    @classmethod
    def post_postprocessor(cls, result=None, **kw):
        pass

    @classmethod
    def patch_single_postprocessor(cls, result=None, **kw):
        pass

    @classmethod
    def patch_many_postprocessor(cls, query=None, data=None, search_params=None, **kw):
        pass

    @classmethod
    def delete_postprocessor(cls, was_deleted=None, **kw):
        pass

    @classmethod
    def delete_many_postprocessor(cls, result=None, search_params=None, **kw):
        pass