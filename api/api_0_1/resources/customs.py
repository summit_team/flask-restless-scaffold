# -*- coding: utf-8 -*-
from flask import Blueprint, request, jsonify

from flask_jwt_extended import create_access_token

from ..models.db import db
from ..models.user import User


custom_api = Blueprint('custom_api', __name__)

@custom_api.route('/auth', methods=['POST'])
def authenticate():
    import hashlib
    from werkzeug.security import safe_str_cmp
    from flask_jwt_extended import create_access_token

    # TODO: validate input
    username = request.json.get('username', None)
    password = request.json.get('password', None)
    user = User.query.filter(User.username == username).first()
    hashed_password = hashlib.sha1(str(password).encode('utf-8')).hexdigest()

    if user is None or not safe_str_cmp(user.password, hashed_password):
        return jsonify({"msg": "Bad username or password"}), 401

    # Identity can be any data that is json serializable
    response = {'access_token': create_access_token(identity=user)}
    return jsonify(response), 200
