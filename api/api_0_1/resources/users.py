from resource import Resource
from flask_restless import ProcessingException
from werkzeug.datastructures import MultiDict
from ..utils.validate_fields import ValidateAllowedFields
import hashlib


class Users(Resource):
    name = 'users'
    include_columns = ['id', 'username', 'email', 'name', 'created', 'updated']
    methods = ['GET', 'POST', 'PATCH', 'DELETE']

    @classmethod
    def post_preprocessor(cls, data=None, **kw):
        from ..wtform_validation import UserForm

        super(Users, cls).post_preprocessor(data=data, **kw)

        allowed_fields = ['username', 'password', 'email', 'name']

        ValidateAllowedFields(allowed_fields, data)

        data_form = MultiDict(mapping=data)

        form = UserForm(data_form)
        if not form.validate():
            raise ProcessingException(description=form.errors)

    @classmethod
    def post_postprocessor(cls, result=None, **kw):

        super(Users, cls).post_postprocessor(result=result, **kw)

    @classmethod
    def patch_single_preprocessor(cls, resource_id=None, data=None, **kw):
        from ..wtform_validation import UpdateUserForm

        super(Users, cls).patch_single_preprocessor(resource_id=resource_id, data=data, **kw)

        allowed_fields = ['email', 'name', 'password']

        ValidateAllowedFields(allowed_fields, data)

        data_form = MultiDict(mapping=data)

        form = UpdateUserForm(data_form)
        if not form.validate():
            raise ProcessingException(description=form.errors)

        if data.get('password'):
            data['password'] = hashlib.sha1(str(data['password'])).hexdigest()

        if not data.get('password'):
            del data['password']

    @classmethod
    def patch_single_postprocessor(cls, result=None, **kw):

        super(Users, cls).patch_single_postprocessor(result=result, **kw)
