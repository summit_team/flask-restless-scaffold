from flask import Flask
from config.config import config
from instance.iconfig import iconfig


def create_app(config_name):
    """
    Generate app based on Flask instance.

    :type config_name: string
    :param config_name: Config Type (i.e. development, testing, production, default).

    :rtype: Object
    :return: Flask Object.
    """
    app = Flask(__name__)

    # Load generic configs
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    # Load specific and sensitive configs
    app.config.from_object(iconfig[config_name])

    # Save environment type
    app.config['ENVIRONMENT'] = config_name


    # Register Blueprint

    from api_0_1.models.db import db
    db.init_app(app)
    db.app = app

    # Register Blueprint for Non RESTfull routes.
    from api_0_1.resources.customs import custom_api as custom_api_0_1_blueprint
    from api_0_1.resources.resource import Resource
    app.register_blueprint(custom_api_0_1_blueprint, url_prefix=Resource.url_prefix)

    from api_0_1 import manager
    manager.init_app(app)

    return app


def get_environment():
    import os

    if 'ENV' in os.environ:
        environment = os.environ['ENV']
    else:
        environment = 'development'

    return environment
