from __init__ import create_app
from __init__ import get_environment
from flask_migrate import Migrate
from api_0_1.models.db import db
from flask_cors import CORS
from flask_jwt_extended import JWTManager
import datetime

environment = get_environment()
app = create_app(environment)
CORS(
    app,
    origins='*',
    supports_credentials=True,
    expose_headers=['Content-Length'],
    allow_headers=['Content-Type', 'Authorization']
)

migrate = Migrate(app, db)


app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(hours=168)
app.secret_key = 'SECRETJWT'
jwt = JWTManager(app)


@jwt.user_identity_loader
def user_identity_lookup(user):
    return user.username


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9000)