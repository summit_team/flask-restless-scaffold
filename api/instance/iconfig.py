import os
import os

# Get current directory & father for logging purposes
current_dir = os.path.abspath(os.path.dirname(__file__))
parent_dir = os.path.abspath(os.path.join(current_dir, os.pardir))


class DefaultInstance():
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class DevelopmentInstance(DefaultInstance):
    DEBUG = True

    LOG = parent_dir + '/logs/'

    LOG_DB = False

    TMP_PATH = '/root/tmp/'

    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://' + str(os.environ.get('MYSQL_USER')) + ':' + str(os.environ.get(
        'MYSQL_PASSWORD')) + '@' + str(os.environ.get('MYSQL_HOSTNAME')) + '/' + str(os.environ.get('MYSQL_DB_NAME'))


    SERVER_SETTINGS = {
        'host': '127.0.0.1',
        'port': 5000
    }


class ProductionInstance(DefaultInstance):
    DEBUG = False

    LOG = parent_dir + '/logs/'

    LOG_DB = False

    TMP_PATH = '/root/tmp/'

    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://' + str(os.environ.get('MYSQL_USER')) + ':' + str(os.environ.get(
        'MYSQL_PASSWORD')) + '@' + str(os.environ.get('MYSQL_HOSTNAME')) + '/' + str(os.environ.get('MYSQL_DB_NAME'))

    SERVER_SETTINGS = {
        'host': '127.0.0.1',
        'port': 5000
    }


iconfig = {
    'development': DevelopmentInstance,
    'production': ProductionInstance,
    'default': DevelopmentInstance
}
